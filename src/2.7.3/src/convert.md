# Convert filter lists

TBlock comes with a built-in filter list converter. If you followed this documentation step-by-step, we have already used this utility once, [when listing all the available filter list formats](./filter-lists/custom.md#specify-the-filter-list-format).

This tool is particularly helpful if you maintain filter lists and want them to be compatible with different ad blockers.

## Convert a filter list

**Convert a filter list (autodetect input format)**
```sh
$ tblockc -C input.txt -o output.txt -s output_format
$ tblockc --convert input.txt --output=output.txt --syntax=output_format
```

**Convert a filter list (specify input format)**
```sh
$ tblockc -C input.txt -i input_format -o output.txt -s output_format
$ tblockc --convert input.txt --input-syntax=input_format --output=output.txt --syntax=output_format
```

**Convert a filter list (with comments)**
```sh
$ tblockc -C input.txt -o output.txt -s output_format -c
$ tblockc --convert input.txt --output=output.txt --syntax=output_format --comments
```

**Convert a filter list (strip blank lines)**
```sh
$ tblockc -C input.txt -o output.txt -s output_format -z
$ tblockc --convert input.txt --output=output.txt --syntax=output_format --optimize
```

## Detect the format of a filter list (without converting it)

```sh
$ tblockc -g list.txt
$ tblockc --get-syntax list.txt
```

## List available formats

```sh
$ tblockc -l
$ tblockc --list-syntax
```
