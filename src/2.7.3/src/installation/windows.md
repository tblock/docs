# Install on Windows

> **&#9888;&#65039; Warning** \
> It seems that a huge hosts file can cause Windows to use a lot of CPU and even become unresponsive.
> It also seems to impact the amount of time required to connect to a network.
> This issue is typically **caused by Windows, not TBlock**.
> We are nevertheless trying to find a way of _reducing this problem_ from our end.
> [More information is available here](https://codeberg.org/tblock/tblock/issues/81).

> **&#8505;&#65039; Info** \
> The user interface is still work-in-progress for Windows and macOS. TBlock is only available as a command-line tool for both of these platforms. [More information is available here](https://codeberg.org/tblock/tblock-gui/issues/1).

## With scoop (recommended)

It is possible to install TBlock on Windows using the [scoop package manager](https://scoop.sh/).

**Add TBlock's third-party repository**
```powershell
> scoop bucket add tblock https://codeberg.org/tblock/bucket
```

**Install the package**
```powershell
> scoop install tblock
```

## Download the installer

TBlock also has a graphical installer available for Windows, available on the releases page.

- [Click here](https://codeberg.org/tblock/tblock/releases) to view releases of `tblock`

> **&#9888;&#65039; Warning** \
> Don't forget to verify the signature of your download if you choose this method.
