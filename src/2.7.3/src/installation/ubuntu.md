# Install on Ubuntu

TBlock has an official PPA available for Ubuntu or Ubuntu-based distributions.

**Enable the PPA**
```sh
$ sudo add-apt-repository ppa:twann4/tblock
```

**Update the repositories**
```sh
$ sudo apt update
```

**Install the package(s)**
```sh
$ sudo apt install tblock
$ sudo apt install tblock-gui
```
