# Install on Arch Linux

> **&#128221; Note** \
> Since version 2.7.0, the package `tblock` contains system services for the following init systems: `systemd`, `openrc`, `runit` and `dinit`.

## From the AUR

There is an AUR package available for `tblock`. You can install it using your favourite AUR helper or by cloning the repository locally.

```sh
$ git clone https://aur.archlinux.org/tblock.git tblock
$ cd tblock
$ makepkg -si
```

A package for `tblock-gui` is also provided on the AUR.

```sh
$ git clone https://aur.archlinux.org/tblock-gui.git tblock-gui
$ cd tblock-gui
$ makepkg -si
```

> **&#128994; Tip** \
> The package `tblock-gui` depends on `tblock`. You should build and install `tblock` before trying to build `tblock-gui`.

## Using Chaotic-AUR

TBlock is also packaged in the [chaotic-aur third-party repository](https://aur.chaotic.cx/).

```sh
$ sudo pacman -S tblock
```

The GUI is also available in this repository:

```sh
$ sudo pacman -S tblock-gui
```
