# Build & install from source

If no package is available for your distribution, you can install TBlock and TBlock GUI from source.

## Build and install `tblock`

**Download and extract the latest stable tarball**
```sh
$ wget https://codeberg.org/tblock/tblock/archive/x.x.x.tar.gz -O tblock.tar.gz
$ tar -xvf tblock.tar.gz
$ cd tblock
```

**Build the package**
```sh
$ make
```

**Install the package (systemd)**
```sh
$ sudo make install install-config
```

**Install the package (OpenRC)**
```sh
$ sudo make install-openrc install-config
```

**Install the package (runit)**
```sh
$ sudo make install-runit install-config
```

**Install the package (dinit)**
```sh
$ sudo make install-dinit install-config
```

## Build and install `tblock-gui`

> **&#128994; Tip** \
> The package `tblock-gui` depends on `tblock`. You should build and install `tblock` before trying to build `tblock-gui`.

**Download and extract the latest stable tarball**
```sh
$ wget https://codeberg.org/tblock/tblock-gui/archive/x.x.x.tar.gz -O tblock-gui.tar.gz
$ tar -xvf tblock-gui.tar.gz
$ cd tblock-gui
```

**Remove dependency on polkit (expert only)**

```sh
$ patch -p1 -i data/patch/no-polkit.patch
```

> **&#9888;&#65039; Warning** \
> If you remove the dependency on polkit, you'll need to call `tblockg` from command-line with root access every time you want to start the GUI.

**Build the package**
```sh
$ make
```

**Install the package**
```sh
$ sudo make install
```
