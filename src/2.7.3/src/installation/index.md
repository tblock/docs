# Installation

## Chapters in this section

1. [Install on Arch Linux](./arch.md)
1. [Install on Debian](./debian.md)
1. [Install on Fedora](./fedora.md)
1. [Install on macOS](./macos.md)
1. [Install on Ubuntu](./ubuntu.md)
1. [Install on Windows](./windows.md)
1. [Build & install from source](./build-from-source.md)
