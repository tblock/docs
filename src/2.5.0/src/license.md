> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# License

## This documentation

This documentation is published under the [GNU Free Documentation License, Version 1.3](https://www.gnu.org/licenses/fdl-1.3):

    Copyright (C)  2022 Twann.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

## TBlock

TBlock is released under the [GNU General Public License, Version 3.0](https://www.gnu.org/licenses/gpl-3.0).

## Website

TBlock's website is released under the [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
