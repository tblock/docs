> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Show help and version information

## Showing help page

Like all command-line tools, it is possible to display a help page for TBlock:

```bash
tblock -h
```

Or:

```bash
tblock --help
```

## Showing version and license information

To show current version and licensing information, run:

```bash
tblock -v
```

Or:

```bash
tblock --version
```
