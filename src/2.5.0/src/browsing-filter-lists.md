> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Browsing filter lists

## Searching

You can search a specific word or tag accross filter lists, just as you would do with a search engine.

To search for filter lists that block tracking, for instance, we would run:

```bash
tblock -Q "tracking"
```

Or:

```bash
tblock --search "tracking"
```

## Getting less information

In automated scripts, you may want to get only the name of the filter lists and not other information about them. That's where the `--quiet` option becomes useful:

```bash
tblock -Qq "tracking"
```

Or:

```bash
tblock --search --quiet "tracking"
```

## Filtering search results

Just as with [the listing operation](listing-filter-lists.md), the four options `--custom`, `--on-repo`, `--subscribing` and `--not-subscribing` can be added to the search operation to filter the results.

For instance, if we wanted to search for blocklists that block ads and that we are not subscribing to, we would run:

```bash
tblock -Qz "ads"
```

Or:

```bash
tblock --search --not-subscribing "ads"
```

If we want to search for filter lists that include `easylist`, that are available in the repository index and that we are subscribing to, we need to run:

```bash
tblock -Qwk "easylist"
```

```bash
tblock --search --on-repo --subscribing "easylist"
```

## Getting information about a specific filter list

We can also display information about a specific filter list (such as name, whether we are subscribing to it, tags, source, license, rules count, etc.). Assuming we want to get information about `easylist`, we have to run:

```bash
tblock -I easylist
```

Or:

```bash
tblock --info easylist
```
