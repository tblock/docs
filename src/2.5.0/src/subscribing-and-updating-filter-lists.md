> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Subscribing and updating filter lists

## Subscribe

To subscribe to a filter list (in this case `easylist`), simply run:

```bash
tblock -S easylist
```

You may use the long operation as well:

```bash
tblock --subscribe easylist
```

## Subscribe to many filter lists

Of course, you can subscribe as many filter lists as you want at the same time, by appending them at the end of the command (also works with long operation):

```bash
tblock -S easylist easyprivacy tblock-base
```

## Subscribe and sync

Since it is [recommended to sync the filter list repository](./syncing-the-filter-list-repository.md#initial-sync) as frequently as possible, you may want to sync the repository while subscribing to filter lists in only one operation:

```bash
tblock -Sy easylist
```

Or the long operation:

```bash
tblock --subscribe --with-sync easylist
```

## Update

Once you've subscribed to the filter lists you want to, it is important to keep them up-to-date as well (for instance if a list contains a false-positive host). To do so, simply run:

```bash
tblock -U
```

Or the long operation:

```bash
tblock --update
```

## Sync and update

Since it is important to keep both repository and filter lists up-to-date, you may want to sync/update them at the same time:

```bash
tblock -Uy
```

And the long operation:

```bash
tblock --update --with-sync
```

## Sync, update and then subscribe

You can also sync the repository, update all filter lists and then subscribe to new lists with a single operation:

```bash
tblock -Syu easyprivacy
```

Or the long version (really long this time):

```bash
tblock --subscribe --with-sync --with-update easyprivacy
```

## Cleaning the cache

When TBlock fetches a filter lists, it automatically stores it in cache. However, you may want to delete cached filter lists to free up some space on your device.
To do so, run:

```bash
tblock -P
```

Or:

```bash
tblock --purge-cache
```
