> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Filter lists

## In this chapter

1. [Types of filter lists and permissions](types-of-filter-lists-and-permissions.md)
1. [Syncing the filter list repository](syncing-the-filter-list-repository.md)
1. [Subscribing and updating filter lists](subscribing-and-updating-filter-lists.md)
1. [Adding custom filter lists](adding-custom-filter-lists.md)
1. [Unsubscribing from filter lists](unsubscribing-from-filter-lists.md)
1. [Managing filter lists](managing-filter-lists.md)