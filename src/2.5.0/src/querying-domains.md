> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Querying domains

Listing user rules is useful, but what if we want to simply check if a rule exists?

A solution would be to run:

```bash
tblock -l | grep "example.org"
```

However, with this solution, we can't see which filter list owns the rule. But it's alright, because TBlock 2.2.0 introduced a new option for this purpose!

Simply run:

```bash
tblock -W example.org
```

Or:

```bash
tblock --which example.org
```
