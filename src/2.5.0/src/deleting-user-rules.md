> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Deleting user rules

## Deleting a set of rules

To delete existing user rules, use the operation:

```bash
tblock -d example.org example.com
```

Or its long alternative:

```bash
tblock --delete-rule example.org example.com
```
