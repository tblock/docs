> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Defining user rules

## Allowing a domain

To allow domains (prevent it from being blocked/redirected by filter lists), run:

```bash
tblock -a example.com example.org
```

Or the long operation:

```bash
tblock --allow example.com example.org
```

## Allow wildcards

With TBlock, you can also add wildcards allowing rules. Here, to allow all subdomains of `example.org` but not `example.org` itself:

```bash
tblock -a *.example.org
```

## Blocking a domain

To block domains, run:

```bash
tblock -b example.com example.org
```

Or the long operation:

```bash
tblock --block example.com example.org
```

## Redirecting a domain

Redirecting domains should not be done, [unless you know exactly what you are doing](how-redirecting-rules-can-be-useful.md). If so, then you can redirect the domains you want to a specific IP address (in this case `0.0.0.0`):

```bash
tblock -r example.com example.org -i 0.0.0.0
```

Or the long operation:

```bash
tblock --redirect example.com example.org --ip 0.0.0.0
```
