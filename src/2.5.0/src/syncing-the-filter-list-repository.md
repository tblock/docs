> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Syncing the filter list repository

## Initial sync

The filter list repository is what makes TBlock unique. With a simple command, users can subscribe to the most popular and powerful lists available. Before being able to subscribe to these [official filter lists](./types-of-filter-lists-and-permissions.md#official-filter-lists), however, it is important to sync the repository, so that TBlock knows what these lists are and where to download them. To do so, you only have to run this as root (or with administration permissions):

```bash
tblock -Y
```

You may also use the long operation:

```bash
tblock --sync
```

Since new filter lists are regularly added to the repository, it is important to keep it up-to-date, by syncing the repository as frequently as possible. 

> **Note:** the [daemon](./daemon.md) allows you to automate the process of syncing the repository.

## Force-sync

By default, if your local version of the repository is up-to-date, nothing will be done. However, you may want to force-sync, to sync it even if it is up-to-date. To do so, simply run:

```bash
tblock -Yf
```

Or the long operation:

```bash
tblock --sync --force
```
