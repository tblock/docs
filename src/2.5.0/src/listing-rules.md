> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Listing rules

## Listing rules

Sometimes, it can be useful to know whether a domain is blocked, allowed, redirected, or if the rule doesn't exist. You may also want to know which rules a specific filter list contains. You can list all rules present in TBlock's database:

```bash
tblock -l
```

Or the long operation:

```bash
tblock --list-rules
```

## Listing user rules only

It is important to see which rules are user rules and which ones aren't, because user rules [have a higher priority than other rules](what-user-rules-are.md#what-user-rules-are). To list all user rules:


```bash
tblock -le
```

The long version:

```bash
tblock --list-rules --user
```

## Listing non-user rules only

You can also list all rules that are set by filter lists, but exclude user rules from them:

```bash
tblock -lt
```

Or:

```bash
tblock --list-rules --standard
```

## Listing rules from specific filter lists only

You can also list the rules from a specific filter list only (or several). Here we'll assume that you want to list the rules from `easylist` and `easyprivacy`:

```bash
tblock -lm easylist easyprivacy
```

With the long version:

```bash
tblock --list-rules --from-filters easylist easyprivacy
```
