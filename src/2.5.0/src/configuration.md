> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Configuration

## In this chapter

1. [Hosts configuration](hosts-configuration.md)
1. [Policy configuration](policy-configuration.md)
1. [Daemon configuration](daemon-configuration.md)
