> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Policy configuration

## Change default filter lists permissions

By default, [filter lists are only allowed to set allowing and blocking rules](types-of-filter-lists-and-permissions.md#permissions). If you want to change this setting, simply write under the `[policy]` section in your config (here to allow filter lists to set allowing and blocking rules by default):

```ini
[policy]
# Change the default permissions to give to different filter lists
# Use A to give Allowing permission, B to give Blocking permission, and R to give Redirecting permission
# Multiple permissions can be given by concatenating them together (i.e. Allowing + Blocking => AB)
# NOTE: This will not affect filter lists you are already subscribing to

# WARNING: it is highly discouraged to give the Redirecting permission to all filter lists here. 
# Instead, it should rather be made manually by using `tblock -M`
# More information here: https://tblock.codeberg.page/docs/faq.html#why-isnt-it-recommended-to-let-filter-lists-to-set-redirecting-rules-by-default

# Default permissions for filter lists that are available in the repository index
regular = AB
```

## Change default custom filter lists permissions

Since TBlock 2.5.0, it is possible to set different permissions for custom filter lists. To do so, simply write under the `[policy]` section:

```ini
# Default permissions for custom filter lists
custom = AB
```

> **Note:** it is discouraged to set the redirecting permissions by default, since this can possibly lead to security flaws on your device. Only give this permissions to filter lists you fully trust!


