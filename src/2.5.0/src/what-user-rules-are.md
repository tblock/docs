> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# What user rules are

User rules are [rules](terminology.md#rule) that are set directly by the user. They have a greater priority than regular rules, which means they overwrite rules that are set by filter lists. They can be useful for allowing domains that are blocked without having to remove a whole filter list.

Exactly like regular rules, they can have three different [policies](terminology.md#policy): `ALLOW`, `BLOCK` and `REDIRECT`.
