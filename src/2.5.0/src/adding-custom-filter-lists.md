> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Adding custom filter lists

## Adding a custom filter list

To add a custom filter list, you have to give it a name that doesn't already exist on the repository. You also need to specify the link/local path of the plain text filter list. To do so:

```bash
tblock -C example-org 'https://example.org/list.txt'
```

You can also use the long operation:

```bash
tblock --add-custom example-org 'https://example.org/list.txt'
```

## Adding many filter lists

You can also subscribe to many custom filter lists at the same time, by appending them at the end of the command (always the name first, and then the link/path of the list):

```bash
tblock -C \
    example-org 'https://example.org/list.txt' \
    local-list '/home/user/blocklist.txt'
```

## Specifying the syntax of a custom filter list

Thanks to TBlock's advanced technology, you usually shouldn't have to worry about the syntax of the custom filter list you are adding. However, with huge lists, you can save some time by specifying manually the syntax (in this example `hosts`):

```bash
tblock -C example-org 'https://example.org/list.txt' -x hosts
```

There is also a long operation available:

```bash
tblock --add-custom example-org 'https://example.org/list.txt' --custom-syntax hosts
```

> **Note:** if you specify the syntax, TBlock will use it for all the custom filter lists that are given in the same command. It means that you can't use this option if you are trying to add two lists that have different formats.

## Renaming a custom filter list

You can rename custom filter lists by running:

```bash
tblock -N id1-old id1-new id2-old id2-new
```

Or:

```bash
tblock --rename id1-old id1-new id2-old id2-new
```
