> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Getting started

## In this chapter

1. [Setting up TBlock](setting-up-tblock.md)
1. [Different profiles](different-profiles.md)
