> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# User rules

## In this chapter

1. [What user rules are](what-user-rules-are.md)
1. [Defining user rules](defining-user-rules.md)
1. [How redirecting rules can be useful](how-redirecting-rules-can-be-useful.md)
1. [Deleting user rules](deleting-user-rules.md)
