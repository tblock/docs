> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Using the converter

## Convert a filter list

The converter allows you to convert a filter list into another format. Here, we'll convert `filter.txt` into the `hosts` format, and write the output to `output.txt`:

```bash
tblockc -C filter.txt -s hosts -o output.txt
```

Or:

```bash
tblockc --convert filter.txt --syntax hosts --output output.txt
```

## Convert and specify the input format

By default, the converter detects the format of the input list. However, since this operation can be slow with huge blocklist, you can specify it manually.
Here, the format of the `filter.txt` is `adblockplus`:

```bash
tblockc -C filter.txt -i adblockplus -s hosts -o output.txt
```

Or:

```bash
tblockc --convert filter.txt --input-syntax adblockplus --syntax hosts --output output.txt
```

## Keep comments

By default, the converter only converts compatible rules. However, you can choose to also convert comments:

```bash
tblockc -Cc filter.txt -s hosts -o output.txt
```

Or:

```bash
tblockc --convert --comments filter.txt --syntax hosts --output output.txt
```
