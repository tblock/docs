> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Terminology

## IP address

An IP address is a suite of numbers from `0` to `255`, seperated by dots (for instance `8.8.8.8`). Every time you connect to the internet, you _ISP_ (_Internet Service Provider_) assigns you a public IP address. This address is unique, and it can be used to access content on the device that has this address. In that case, the device is called a server.

## DNS

_DNS_ stands for _Domain Name System_. It is a service that is used by computers to connect to websites. When a computer tries to connect to `example.org`, it sends a request to a _DNS_ server, which sends a response containing the IP address of `example.org`. The connection from the computer to example.org is now possible.

## Hosts file

The hosts file is a file that is present on all operating systems. It acts like a DNS, but has a higher priority than the DNS. It can be used to redirect hosts (such as `example.org`) to custom IP addresses.

## Database

As the name suggests, a database is simply an efficient way to store data.

## Filter list

A filter list is a text file, containing a list of domains (for instance `example.com`), that can be used by an ad-blocker to block all domains that are on it. Depending on the format, it is possible to also whitelist some domains inside a filter list. Whithout filter lists, ad blocking wouldn't be possible.

## Rule

A rule is a way to tell an ad-blocker what to do about a specific domain. In fact, a filter list is a set of rules, that tells the ad-blocker which domains to block and which not to block.

## Filter list format / syntax

The format (or syntax) of a filter list is simply a way to write rules inside the list so that the ad-blocker understands what to do with the specified hosts. Different ad-blockers use different formats. For instance, Adblock Plus uses an advanced syntax that allows to block hosts under certain conditions. This is not possible with others, such as AdAway or pi-hole, or TBlock. However, TBlock supports the most popular filter lists. It means the users should not have to worry about that.

## Policy

A policy is the instruction a rule gives to the ad-blocker about a specific host. A policy can either be `ALLOW`, `BLOCK` or `REDIRECT`. If the policy is `ALLOW`, then the host is whitelisted. If the policy is `BLOCK`, the host is blocked, and if it is `REDIRECT`, the domain is redirected to the ip address specified in the rule.

## Daemon

In computing, a daemon is a program that runs in the background, performing invisible but yet important tasks for the daily user. Without daemons, it wouldn't be possible to have a graphical desktop.
