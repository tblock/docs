> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# More operations

## In this chapter

1. [Toggling protection](toggling-protection.md)
1. [Getting status information](getting-status-information.md)
1. [Show help and version information](show-help-and-version-information.md)
