> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Listing filter lists

Just as you can list rules, you can also list filter lists.

## Listing all filter lists

When you list filter lists, all filter lists are shown by default (both the ones available on the repository and the ones you added manually):

```bash
tblock -L
```

Or:

```bash
tblock --list
```

## Listing custom filter lists only

To list only the filter lists that have been manually added by the user, run:

```bash
tblock -Lc
```

Or the long version:

```bash
tblock --list --custom
```

## Listing only non-custom filter lists

To list only filter lists that are in the repository index:

```bash
tblock -Lw
```

Or, with the long operation:

```bash
tblock --list --on-repo
```

## Listing only subscribed filter lists

You can also list only the filter lists you are subscribing to:

```bash
tblock -Lk
```

Or:

```bash
tblock --list --subscribing
```

## Listing only not-subscribed filter lists

You can also list only the filter lists you are **not** subscribing to:

```bash
tblock -Lz
```

Or:

```bash
tblock --list --not-subscribing
```

## Listing non-custom and subscribed filter lists

The options can be combined together. For instance, to list all filter lists that are both available in the repository index and subscribed:

```bash
tblock -Lwk
```

Or:

```bash
tblock --list --on-repo --subscribing
```

## Listing non-custom and not-subcribed filter lists

To list only filter lists that are both available in the repository index and not subscribed:

```bash
tblock -Lwz
```

Or:

```bash
tblock --list --on-repo --not-subscribing
```

## Getting less information

In automated scripts, you may want to get only the name of the filter lists and not other information about them. That's where the `--quiet` option becomes useful:

```bash
tblock -Lq
```

Or:

```bash
tblock --list --quiet
```
