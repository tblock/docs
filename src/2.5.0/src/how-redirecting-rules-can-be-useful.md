> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# How redirecting rules can be useful

Usually, it is not recommended to use redirecting rules since it may compromise your network traffic. 
However, they can still be useful for advanced user. Here is an example where redirecting rules are really helpful.

## Mapping your devices on a local area network (LAN)

Let's say that you own a Raspberry Pi that you've manually configured to always have the same IP address on your local network.
The address is probably something like `192.168.250.67`. 

However, if you want to access it remotely from your desktop computer, you'll have to enter the IP address every time you want to initiate a ssh connection to it. That's where TBlock can help you. Instead of having to remember the complicated IP address, you can give the domain name that you want to your local device. For instance:

```bash
tblock -r rpi.local -i 192.168.250.67
```
Or:
```bash
tblock --redirect rpi.local --ip=192.168.250.67
```

This will allow you to access your Raspberry Pi under the domain name `rpi.local` instead of the complicated IP address.
