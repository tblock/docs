> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Toggling protection

## Refreshing rules

If your hosts file was modified, you may want TBlock to refresh its rules (update the hosts file without updating your filter lists). To do so:

```bash
tblock -H
```

The long operation:

```bash
tblock --update-hosts
```

## Disabling protection

To disable the protection and restore your default hosts file, run:

```bash
tblock -D
```

With the long operation:

```bash
tblock --disable
```

## Enabling protection

If you disabled TBlock, it is easy to enable its protection again:

```bash
tblock -E
```

Or:

```bash
tblock --enable
```
