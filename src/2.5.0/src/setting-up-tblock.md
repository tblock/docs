> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Setting up TBlock

After installing TBlock, it is recommended to use the setup wizard that was introduced in TBlock 2.5.0. To do so, run:

```bash
tblock --init
```

Or:

```bash
tblock -1
```

Then, the program will ask you to [choose a profile](different-profiles.md). When you're done, the setup should complete and TBlock will be ready to use. Simple, isn't it?
