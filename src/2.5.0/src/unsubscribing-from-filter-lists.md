> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Unsubscribing from filter lists

## Unsubscribe

If you don't want to subscribe to a filter list anymore, you can easily remove it (in this case `easylist`):

```bash
tblock -R easylist
```

You may want to use the long operation: 

```bash
tblock --remove easylist
```

> **Note:** just as subscribing, you can remove several filter lists at the same time by passing them after the `--remove` operation.

## Unsubscribe and update

Once a filter list is removed, so are its rules. However, if another filter list blocks the same domains than the removed list, you need to update your lists for the rules to be restored. You can do so by running:

```bash
tblock -Ru easylist
```

Or the long operation:

```bash
tblock --remove --with-update easylist
```