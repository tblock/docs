> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

![Banner](./banner.webp)

# Introduction

Welcome to TBlock documentation!

This is the place where you can learn how to use TBlock and how it works. If this is your first time using TBlock, we recommend you to read this documentation from the beginning, but you can also select the chapter you are interested into.

> This documentation is available for TBlock 2.5.0 or later. Information for older versions may however be inaccurate.

## Chapters summary

1. [Introduction](introduction.md)
1. [Getting started](getting-started.md)
1. [Filter lists](filter-lists.md)
1. [User rules](user-rules.md)
1. [Getting information](getting-information.md)
1. [More operations](more-operations.md)
1. [Daemon](daemon.md)
1. [Filter list converter](filter-list-converter.md)
1. [Configuration](configuration.md)
1. [TBlock filter format](tblock-filter-format.md)
1. [Troubleshooting](troubleshooting.md)
1. [FAQ](faq.md)
1. [License](license.md)

## In this chapter

1. [Terminology](terminology.md)
1. [How it works](how-it-works.md)
