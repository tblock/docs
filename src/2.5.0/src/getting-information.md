> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Getting information

## In this chapter

1. [Listing rules](listing-rules.md)
1. [Querying domains](querying-domains.md)
1. [Listing filter lists](listing-filter-lists.md)
1. [Browsing filter lists](browsing-filter-lists.md)
