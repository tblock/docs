> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# FAQ

## Why do I still see ads after installing and setting up TBlock?

Since TBlock uses the hosts file, it gives you a systemwide protection. However, it can't block specific scripts like browser addons do. It means that you may still see some sneaky ads. The only thing you can do is to subscribe to more and to bigger filter lists. You may also want to install [uBlock Origin](https://github.com/gorhill/uBlock) in addition to TBlock.

## Is there a graphical interface for TBlock?

Yes! However, it is currently in Open Alpha, which means that there is no stable release right now. If you like, you can help us testing and finding bugs. More information are available [on Codeberg](https://codeberg.org/tblock/tblock-gui).

## Why isn't it recommended to let filter lists to set redirecting rules by default?

Simply because that means that they would by able to redirect any domain to any server they want. This could cause compromise all your network traffic.

For example, a filter list could redirect your mail server to a phishing website, and you wouldn't even notice. This is why it is recommended to allow only trusted filter lists to set redirecting rules.

You can check at any time if there are any redirecting rules set by running:

```sh
tblock -l | grep "REDIRECT"
```

## I think I found an issue. What should I do?

It depends. If the issue you found is a vulnerability, than please [follow carefully the instructions written here](https://codeberg.org/tblock/tblock/src/branch/main/.gitea/issue_template/vulnerability.md). Otherwise, [have a look at this document that contains useful information for reporting issues](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTING.md).

## How to backup my filter lists?

If you want to make a backup of the filter lists you subscribe to, you can simply run the following:

```sh
tblock -Lkq > tblock-backup.txt
```

Then, to restore it:

```sh
tblock -Sy $(cat tblock-backup.txt)
```

