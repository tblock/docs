> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Starting the daemon

## Starting a single instance

To start an instance of the daemon, simply run (assuming your configuration is located under `/etc/tblock.conf`):

```bash
tblockd -dc /etc/tblock.conf
```

Or the long alternative:

```bash
tblockd --daemon --config=/etc/tblock.conf
```

## Starting another instance

If an instance of the daemon is already running, you are not able to start another instance by default. However, you can do that by telling the new instance to ignore the PID file:

```bash
tblockd -dnc /etc/tblock.conf
```

Or with the long version:

```bash
tblockd --daemon --no-pid --config=/etc/tblock.conf
```
