> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Managing filter lists permissions

## Change the permissions

By default, filter lists are only allowed to define blocking rules. However, you can grant them the permission to define allowing or redirecting rules.

To grant a filter list allowing and blocking permissions (in this example `easylist`):

```bash
tblock -M easylist -p AB
```

Or the long operation:

```bash
tblock --mod easylist --permissions AB
```

> **Note**: the letter `A` stands for **A**llowing rules, `B` for **B**locking rules, and `R` for **R**edirecting rules.

To grant a filter list allowing and redirecting permission only:

```bash
tblock -M easylist -p AR
```

> **Note:** redirecting rules can compromise your network traffic and lead to MITM attacks. Be sure to fully trust the filter lists you grant the "redirecting" permission.

To restore the default permissions (blocking only):

```bash
tblock -M easylist -p B
```
