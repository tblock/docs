> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Troubleshooting

Here is a list of the common issues

## Domains are not blocked

If you use Tor Browser or if your browser uses DNS-over-HTTPS, it will interfere with TBlock. Since TBlock acts like a DNS server, it can't be used if the browser proxies the DNS requests. You'll have to disable your browser's settings, or, in the case of Tor Browser, simply don't care.

## Error: database is locked

Sometimes, when you run an operation with TBlock, you see an error saying:

```
Error: database is locked, please wait for other processes to terminate.
If you are sure that the daemon or any other instances are not running, you can delete:
-> /var/lib/tblock/.db_lock
```

What it means is that there is probably another process that is using TBlock's database at the time. Usually, it's [the daemon](daemon.md) that is simply updating your filter lists. However, if TBlock recently crashed, it can also be a side-effect of the crash. In that case, and after checking that there are no other processes running, you can delete `/var/lib/tblock/.db_lock` to fix this issue.

## tblock module not found

When running TBlock as root, you can possibly get an error message such as:

```txt
importlib.metadata.PackageNotFoundError: No package metadata was found for tblock
```
```txt
ModuleNotFoundError: No module named 'tblock'
```

This is usually caused by the fact that you installed TBlock using `pip` and its `--user` option. To solve that, follow the steps below.

First, remove the Python package:
```sh
pip uninstall --user tblock
```

Next, install it again [with `pipx`](https://tblock.codeberg.page/install/python) or [any other installation method](https://tblock.codeberg.page/install).

## Daemon is unable to start: /usr/bin/tblockd does not exist

Depeding on the installation method you choose, the daemon might not be installed under `/usr/bin/tblockd`, but rather under `/usr/bin/local/bin/tblockd`.

To fix that, you simply have to create a symbolic link under `/usr/bin/tblockd`, pointing to the real executable:

```
ln -s /usr/local/bin/tblockd /usr/bin/tblockd
```

## The daemon does keeps updating every 240 minutes, even though I specified a lower value in the configuration file

This bug is usually caused by the fact that you specified a value that's too low for the daemon in your configuration file. The lowest value supported by the daemon is 60 minutes. If the value you specified is lower than 60 minutes, it will automatically change it to 240 minutes (which is the default value).
