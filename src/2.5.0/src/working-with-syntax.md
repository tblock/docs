> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Working with syntax

## Listing available formats

To list available filter list formats/syntax, run:

```bash
tblockc -l
```

Or:

```bash
tblockc --list-syntax
```

## Get the format of a file

To detect the format/syntax of a filter list, run (assuming the file you want to check is `list.txt`):

```bash
tblockc -g list.txt
```

Or the long operation:

```bash
tblockc --get-syntax list.txt
```
