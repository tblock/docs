> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Different profiles

Profiles are a new feature that exists since TBlock 2.5.0. It allows users to subscribe to a pre-defined set of filter lists to help them configure TBlock in the easiest way possible.

Currently, four different profiles exist:

---

**None**

This profile doesn't subscribe to any filter list, it just [updates the repository index](syncing-the-filter-list-repository.md) to make it easier to subscribe to filter lists afterwards.

---

**Light**

While some people may consider it sufficient, some ads and trackers won't be blocked with this profile.
This profiles subscribes to the following filter lists:

- [peter-lowe](https://tblock.codeberg.page/repository/?filter=peter-lowe)
- [stevenblack-hosts](https://tblock.codeberg.page/repository/?filter=stevenblack-hosts)
- [tblock-base](https://tblock.codeberg.page/repository/?filter=tblock-base)

---

**Balanced**

This is the default profile for new users, that keeps a good balance between performance and blocking score.
It subscribes to the following filter lists:

- [adguard-cname](https://tblock.codeberg.page/repository/?filter=adguard-cname)
- [adguard-dns](https://tblock.codeberg.page/repository/?filter=adguard-dns)
- [adguard-tracking-servers](https://tblock.codeberg.page/repository/?filter=adguard-tracking-servers)
- [cpbl-filters](https://tblock.codeberg.page/repository/?filter=cpbl-filters)
- [peter-lowe](https://tblock.codeberg.page/repository/?filter=peter-lowe)
- [stevenblack-hosts](https://tblock.codeberg.page/repository/?filter=stevenblack-hosts)
- [tblock-base](https://tblock.codeberg.page/repository/?filter=tblock-base)
- [tblock-security](https://tblock.codeberg.page/repository/?filter=tblock-security)

---

**Aggressive**

As the name suggests, this profile prioritizes high blocking score over websites appearance, and it may break some web pages or apps.
It subscribes to the following filter lists:

- [adguard-cname](https://tblock.codeberg.page/repository/?filter=adguard-cname)
- [adguard-dns](https://tblock.codeberg.page/repository/?filter=adguard-dns)
- [adguard-tracking-servers](https://tblock.codeberg.page/repository/?filter=adguard-tracking-servers)
- [cpbl-filters](https://tblock.codeberg.page/repository/?filter=cpbl-filters)
- [ddgtrackerradar](https://tblock.codeberg.page/repository/?filter=ddgtrackerradar)
- [divested](https://tblock.codeberg.page/repository/?filter=ddgtrackerradar)
- [mpvs-hosts](https://tblock.codeberg.page/repository/?filter=mpvs-hosts)
- [peter-lowe](https://tblock.codeberg.page/repository/?filter=peter-lowe)
- [stevenblack-hosts](https://tblock.codeberg.page/repository/?filter=stevenblack-hosts)
- [tblock-base](https://tblock.codeberg.page/repository/?filter=tblock-base)
- [tblock-security](https://tblock.codeberg.page/repository/?filter=tblock-security)

---
