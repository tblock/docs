> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Daemon

## In this chapter

1. [Daemon quick overview](daemon-quick-overview.md)
1. [Starting the daemon](starting-the-daemon.md)
