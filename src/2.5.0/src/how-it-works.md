> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# How it works

## Filter list repository

As we already said, filter lists are essential for an efficient protection against advertising and tracking. However, finding these filter lists may be difficult for a new user. That's why TBlock has [an official filter list repository](https://tblock.codeberg.page/repository), allowing the user to use them just with one simple operation. Almost all filter lists available on the repository are also mirrored by the team behind TBlock, to improve decentralization in case a server is down.

## Hosts file and localhost

To block domains, TBlock uses the hosts file. With it, it redirects all blocked domains to the local IP address, `0.0.0.0`. That way, when a program or a web page tries to connect to a blocked domain, it connects to the local address instead. It means that the requested tracking script or advert is not available and thus not loaded. 
