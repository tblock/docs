> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Filter list converter

## In this chapter

1. [Introduction to the converter](introduction-to-the-converter.md)
1. [Working with syntax](working-with-syntax.md)
1. [Using the converter](using-the-converter.md)
