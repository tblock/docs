> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Getting status information

If we want to see if TBlock is active or not, the total rules count, or the number of active filter lists, we can do so by running:

```bash
tblock -s
```

Or the long operation:

```bash
tblock --status
```
