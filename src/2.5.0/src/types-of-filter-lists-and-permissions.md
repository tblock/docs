> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Types of filter lists and permissions

## Official filter lists

Official filter lists are filter lists that can be found on [the filter list repository](https://tblock.codeberg.page/repository/). Some details (such as syntax, license and homepage) about them are also provided.

## Custom filter lists

A custom filter list is a filter list that is not available on the repository, that the user had to add manually.

## Permissions

Permissions are a way to control which [rule policies](./terminology.md#policy) a specific filter list is allowed to use. By default, filter lists are only allowed to set blocking rules.
