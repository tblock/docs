> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Daemon configuration

## Syncing the repository index

By default, the daemon updates both the filter lists and the repository index. If you want to change this setting, write:

```ini
[daemon]
# Change the behavior of the daemon
# NOTE: You need to restart the daemon for these changes to apply

# Sync the repository when updating filter lists
sync_repo = false
```

## Changing the update frequency

By default, filter lists are updated every 4 hours. You can change this value (for instance to 8 hours) by writing under the `[daemon]` section of your config:

```ini
# How often the filter lists should be updated (value expressed in minutes)
frequency = 480
```

> **Note:** the time specified here is in minutes. For one hour, we will write `60`, for two `120`, etc.

## Force-update

By default, filter lists are updated normally. If you want to force-update instead, edit the following line under the `[daemon]` section:

```ini
# Force-update filter lists (this is usually not needed)
force = true
```

## Protection against hosts file modification

By default, the daemon prevents other programs or users from editing your hosts file while running. This improves the security of your device, but you may want to disable it. To do so, simply write the following line under the `[daemon]` section:

```ini
# Detect changes made to the hosts file by other programs/others and overwrite them
anti_hijack = false
```
