> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Introduction to the converter

The converter is yet another great feature provided by TBlock. It allows users to easily convert filter lists from a syntax to another one. This can be useful for distributing home-made lists, while assuring their compatiblity with different ad-blockers.
