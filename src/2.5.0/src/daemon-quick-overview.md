> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Daemon quick overview

## History & use

The daemon was introduced in TBlock version 2.0.0. It is a simple utility that runs in background and that can perform the following tasks:

- Update the filter lists (and sync the repository index) at a regular time interval
- Protect the hosts file against being edited by another program or user

## Multiple instances

By default, only one instance of the daemon can run at the same time. When an instance of the daemon starts, it creates a PID file, and, if the file already exists then it fails to start. The PID file is removed when the instance of the running daemon stops.
