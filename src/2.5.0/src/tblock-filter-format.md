> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# TBlock filter format

Even though TBlock supports a wide variety of filter list formats, not all formats are 100% compatible with TBlock. For instance, the hosts file format doesn't support allowing rules, and the AdblockPlus format doesn't support redirecting a domain to a given IP address. TBlock filter list format aims to be a simple way to write filter lists that are fully supported by TBlock.

## Defining rules

Rules are written under a specific section that tells TBlock what to do with the given hosts. For instance, to allow some domains, they must be written inside the `allow` section:

```toml
[allow]
example.org
example.com
```

To block domains, they must go inside the `block` section:

```toml
[block]
example.org
example.com
```

Now, to redirect domains, it's not really complicated either. You just have to include the IP address where the domains should be redirected **inside the `redirect` section definition**:

```toml
[redirect "127.0.0.2"]
example.org
example.com
```

> **Note:** you can also specify an IPv6 address inside the section definition.

## Adding comments

Like most ad-blockers filter list formats, you can add comments by writting `#` as the first character of a line:

```toml
# This is a comment
```

## Final thoughts

You can, of course, have multiple sections inside a single filter list. However, to keep it as little as possible, it is recommended to only have one section for each policy.

For instance, you should rather do that:

```toml
# This is good

[allow]
example.org
example.com

[block]
ns1.example.org
ns2.example.org

[redirect "127.0.0.2"]
ns1.example.com

[redirect "::1"]
ns2.example.com
```

Than that:

```toml
# This is BAD

[allow]
example.org

[block]
ns1.example.org

[allow]
example.com

[block]
ns2.example.org

[redirect "127.0.0.2"]
ns1.example.com

[redirect "::1"]
ns2.example.com
```
