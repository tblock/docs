> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.6.1 or later](/2.6.1/).

# Hosts configuration

TBlock's configuration file is located under `/etc/tblock.conf` on GNU/Linux and under `"%ALLUSERSPROFILE%\TBlock\conf.ini` on Windows. It can be used to modify TBlock's behavior.

## Change default blocking address

By default, [TBlock redirects blocked domains to `0.0.0.0`](how-it-works.md#hosts-file-and-localhost). However, if you want to change this value, for instance to `127.0.0.1`, you just need to write this line under the `[hosts]` section of your config:

```ini
[hosts]
# Change the way the hosts file is built by TBlock
# NOTE: You need to rebuild your hosts file with `tblock -H` for these changes to apply

# Change the default IPv4 redirecting address for blocked domains
default_ip = 127.0.0.1

```

## Enable IPv6 blocking

By default, TBlock redirects blocked domains to the local IPv4 address only. If you want to enable redirecting to IPv6, write the following line (still under the `[hosts]` section):

```ini
# Allow IPv6 rules and IPv6-based blocking
# WARNING: this will cause your hosts file to be twice as big as before
allow_ipv6 = false
```

> **Note:** your hosts file will be twice bigger than before if you enable this setting.

## Change default IPv6 blocking address

Just as we can change the default IPv4 blocking address, we can also change the IPv6 one, by writing under the `[hosts]` section:

```ini
# Change the default IPv6 redirecting address for blocked domains
# This will only be used if `allow_ipv6` is set to `true`
default_ipv6 = ::1
```
