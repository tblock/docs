> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/user-rules.html).

# User rules

Another great feature available with TBlock is the ability to define *user rules*. These rules are almost identical to rules that are found in filter lists. The only difference is that they have a higher priority than other rules.

A *user rule* will overwrite any existing rule.

## Allow a domain

Allowing a domain means preventing it from being blocked by filter lists. A domain can be allowed using:

```sh
$ tblock -a example.org
$ tblock --allow example.org
```

## Block a domain

Blocking a domain means adding an entry to the hosts file that will cause it to resolve as `0.0.0.0`. When a domain is blocked, it is impossible to connect to it. To block a domain:

```sh
$ tblock -b example.org
$ tblock --block example.org
```

## Redirect a domain

> **&#9940; DANGER** \
> Proceed with great caution when redirecting a domain, since it has the potential to compromise your network traffic. You should only use this feature if you know exactly what you are doing.

It is also possible to add a custom entry to the hosts file that will cause a domain to resolve as an IP address different than the one it should normally. We can do this using this command (here we want `example.org` to resolve as `127.0.0.1`):

```sh
$ tblock -r example.org -i 127.0.0.1
$ tblock --redirect example.org --ip=127.0.0.1
```

## Delete a *user rule*

> **&#128221; Note** \
> It is only possible to delete *user rules* using this operation.

When a user rule is no longer needed, it can be deleted with:

```sh
$ tblock -d example.org
$ tblock --delete-rule example.org
```

It means that filter lists will be able to define rules for `example.org` again.

## List user rules

We can list all user rules with:

```sh
$ tblock -le
$ tblock --list-rules --user
```
