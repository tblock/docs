> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/common-issues.html).

# Common issues

## TBlock was flagged as malware on Windows

Windows or other antivirus software often flag TBlock as a malware. They're wrong. TBlock is [open source](https://codeberg.org/tblock/tblock), so you can check by yourself if you don't believe us. The two main reasons Windows and antivirus don't like TBlock is that:

1. The binaries are not signed (because it costs money and we currently do not own enough for this). However, if you verified the signature of your download, this should not be a problem.
1. TBlock needs to modify a system file (the *hosts file*) in order to work. Some malwares have used the hosts file in the past for phishing purposes, which explains the stress caused to Windows and antivirus software.

If your antivirus flagged TBlock as malware, you'll have to add an exception for it in order to use it.

## Domains are not blocked

If you use Tor Browser or if your browser uses DNS-over-HTTPS, it will interfere with TBlock. Since TBlock acts like a DNS server, it can't be used if the browser proxies the DNS requests. You'll have to disable your browser's settings, or, in the case of Tor Browser, simply don't care.

## Error: database is locked

Sometimes, when we try to run an operation with TBlock, we may see an error saying:

```text
Error: database is locked, please wait for other processes to terminate.
If you are sure that the daemon or any other instances are not running, you can delete:
-> /var/lib/tblock/.db_lock
```

What it means is that there is probably another process that is using TBlock's database at the time. Usually, it's automatic updates that are being made. However, if TBlock recently crashed, it can also be a side-effect of the crash. In that case, and after checking that there are no other processes running, we can delete `/var/lib/tblock/.db_lock` to fix this issue.
