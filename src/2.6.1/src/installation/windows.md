> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/windows.html).

# Install on Windows

> **&#8505;&#65039; Info** \
> The user interface is still work-in-progress for Windows and macOS. TBlock is only available as a command-line tool for both of these platforms. [More information is available here](https://codeberg.org/tblock/tblock-gui/issues/1).

## With scoop (recommended)

It is possible to install TBlock on Windows using the [scoop package manager](https://scoop.sh/).

**Add TBlock's third-party repository**
```powershell
> scoop bucket add tblock https://codeberg.org/tblock/bucket
```

**Install the package**
```powershell
> scoop install tblock
```

## Download the installer

TBlock also has a graphical installer available for Windows, available on the releases page.

- [Click here](https://codeberg.org/tblock/tblock/releases) to view releases of `tblock`

> **&#9888;&#65039; Warning** \
> Don't forget to verify the signature of your download if you choose this method.
