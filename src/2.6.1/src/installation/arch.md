> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/arch.html).

# Install on Arch Linux

## From the AUR

There is an AUR package available for `tblock`. You can install it using your favourite AUR helper or by cloning the repository locally.

```sh
$ git clone https://aur.archlinux.org/tblock.git tblock
$ cd tblock
$ makepkg -si
```

A package for `tblock-gui` is also provided on the AUR.

```sh
$ git clone https://aur.archlinux.org/tblock-gui.git tblock-gui
$ cd tblock-gui
$ makepkg -si
```

> **&#128994; Tip** \
> The package `tblock-gui` depends on `tblock`. You should build and install `tblock` before trying to build `tblock-gui`.

## Using Chaotic-AUR

TBlock is also packaged on the [chaotic-aur third-party repository](https://aur.chaotic.cx/).

```sh
$ sudo pacman -S tblock
```

> **&#10067; What about the GUI?** \
> TBlock GUI is currently not available on the Chaotic AUR repository. You have to install it using the AUR.
