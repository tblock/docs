> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/arch.html).

# Install on Artix Linux

TBlock doesn't directly depend on `systemd`. However, if you want automatic updates to work correctly, you need an init system. The `tblock` package uses `systemd` by default.
If you use another init system, you can install the correct package for you system. All of them are available on the AUR.

> **&#128221; Note** \
> The package `tblock-gui` has no direct dependency on any init system. You can install it on any distribution.

## OpenRC package

You need to install the package `tblock-openrc` instead of `tblock`:

```sh
$ git clone https://aur.archlinux.org/tblock-openrc.git tblock
$ cd tblock
$ makepkg -si
```

## runit package

You need to install the package `tblock-runit` instead of `tblock`:

```sh
$ git clone https://aur.archlinux.org/tblock-runit.git tblock
$ cd tblock
$ makepkg -si
```

## dinit package

You need to install the package `tblock-dinit` instead of `tblock`:

```sh
$ git clone https://aur.archlinux.org/tblock-dinit.git tblock
$ cd tblock
$ makepkg -si
```

## Other init systems

TBlock currently does not support other init systems. You can install `tblock`, but you won't be able to have automatic updates.
If you want, you can write a system service and open a pull request to get it included in the project. It would be much appreciated.
