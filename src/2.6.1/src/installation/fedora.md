> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/fedora.html).

# Install on Fedora

If you are running an Fedora-based distribution, you can enable TBlock's third-party repository to install TBlock and to receive updates in an easy way.

**Enable TBlock's COPR**
```sh
$ sudo dnf copr enable twann/tblock
```

**Install the package(s)**
```sh
$ sudo dnf install tblock
$ sudo dnf install tblock-gui
```