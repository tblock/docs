> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/debian.html).

# Install on Devuan

TBlock doesn't directly depend on `systemd`. However, if you want automatic updates to work correctly, you need an init system. The `tblock` package uses `systemd` by default.
If you use another init system, you can install the correct package for you system. All of them are available on the releases page.

> **&#128221; Note** \
> The package `tblock-gui` has no direct dependency on any init system. You can install it on any distribution.

- [Click here](https://codeberg.org/tblock/tblock/releases) to view releases
