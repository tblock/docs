> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/installation/macos.html).

# Install on macOS

> **&#9888;&#65039; Warning** \
> TBlock hasn't yet been officially tested on macOS, so there might be unresolved issues. If you do have TBlock installed on your macOS system, your feedback would be very much appreciated.

> **&#8505;&#65039; Info** \
> The user interface is still work-in-progress for Windows and macOS. TBlock is only available as a command-line tool for both of these platforms. [More information is available here](https://codeberg.org/tblock/tblock-gui/issues/1).

## Installation

If you are using macOS, you can install TBlock with the [homebrew package manager](https://brew.sh/).

**Enable TBlock's third-party repository**
```sh
$ brew tap tblock/tap https://codeberg.org/tblock/homebrew-tap
```

**Install the package**
```sh
$ brew install tblock
```

## Troubleshooting

After installing, you might encounter the following error:

```sh
$ sudo tblock --init
sudo: tblock: command not found
```

Just append the following line to `~/.bashrc` or `~/.zshrc`, depending on your shell:

```sh
alias sudo="PATH=$PATH:$(brew --prefix)/bin:$(brew --prefix)/sbin sudo"
```
