> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/manage-protection.html).

# Manage protection

## Introduction

TBlock is able to block unwanted websites by using something called the *hosts file*. When connecting to a website (for example `example.org`), our system queries a *DNS server*, which then responds by sending the *IP address* of the website (in that case, `93.184.216.34`), allowing us to connect to it. This process is called *resolving*.

The *hosts file* is part of every operating system and allows us to resolve a domain name without having to use DNS. When blocking a website, TBlock simply adds an entry to this file, telling it to resolve the domain name with the IP address `0.0.0.0`, which is a local address (it is not available through the internet). That way, when an application or a website is trying to connect to a blocked domain, it connects to the local host and can't fetch the requested resource (ad, tracking script, etc.).

## Disable protection

Sometimes, we need to disable protection for a few minutes. When we do that, TBlock restores the *stock* hosts file (the one that we had before using TBlock). In order to do that, we need to execute:

```sh
$ sudo tblock -D
$ sudo tblock --disable
```

## Enable protection

If you followed the [post-installation steps](./post-installation.md) correctly, the protection should already be enabled by default. However, when we disable the protection, we need to enable it again after. It is possible to do so by running:

```sh
$ sudo tblock -E
$ sudo tblock --enable
```

> **&#10060; Failed to enable protection?** \
> It is possible that you get an error similar to this one while trying to enable the protection: 
> ```text
> ==> Applying built hosts file
> [x] Checking checksum
> [i] Run tblock -H to build the hosts file
> ```
> This can easily be fixed by [rebuilding the hosts file](#build-the-hosts-file).

## Get status information

At any time, you can check the status (i.e. whether the TBlock is active or not, the number of filter lists subscribtions, the number of rules, whether automatic updates are enabled, etc.) by using this command:

```sh
$ tblock -s
$ tblock --status
```

## Build the hosts file

Sometimes we need to completely re-build the hosts file with all the blocked entries. This is usually not needed (because done automatically), but it can be useful to do it manually in some cases (i.e. when errors are encountered while trying to enable the protection). We can build a fresh hosts file with:

```text
$ sudo tblock -H
$ sudo tblock --update-hosts
```
