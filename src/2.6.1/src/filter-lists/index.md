> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/filter-lists/).

# Filter lists

## Chapters in this section

1. [Filter list repository](./repository.md)
1. [Managing filter lists](./manage.md)
1. [Custom filter lists](./custom.md)
