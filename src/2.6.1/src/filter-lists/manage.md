> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/filter-lists/manage.html).

# Manage filter lists

Filter lists are text files that contain a list of domain and the information of what to do with these domains. While most filter lists contain *blocking* rules, some of them also contain *allowing* rules.

- Domains that are blocked are mapped to `0.0.0.0` in the hosts file.
- Domains that are allowed cannot be blocked by any other filter list.

## Subscribe

To use filter lists, we need to subscribe to them. When we subscribe to a list, TBlock will apply its rules to the hosts file.

If you followed the [post-installation instructions](../post-installation.md#set-up-the-protection), you should already be subscribing to a bunch of filter lists. You can list the filter lists you are subscribing to by executing:

```sh
$ tblock -Lk
$ tblock --list --subscribing
```

When subscribing to a filter list that is available in the filter list repository, we need to indicate its ID (or name). A common example is to subscribe to `tblock-base`, which is the base filter list for effective ads and trackers blocking:

```sh
$ tblock -S tblock-base
$ tblock --subscribe tblock-base
```

We can also subscribe to several filter lists at the same time (here `easylist`, `easyprivacy` and `stevenblack-hosts`):

```sh
$ tblock -S easylist easyprivacy stevenblack-hosts
$ tblock --subscribe easylist easyprivacy stevenblack-hosts
```

## Unsubscribe

If we are subscribing to a filter list that we no longer need, we can easily remove it from our subscriptions. Here, with `tblock-base`:

```sh
$ tblock -R tblock-base
$ tblock --remove tblock-base
```

## Update

> **&#8505;&#65039; Info** \
> If we have automatic updates enabled, we usually don't need to update filter lists manually.

It is common that some filter lists contain some false positives, or that new ads/tracking servers are created. That's why it is very important to regularly update your filter lists. It is really simple to do, but it can take some time, depending on how many filter lists you are subscribing to.

```sh
$ tblock -U
$ tblock --update
```

It is also possible to update both the filter list repository and the filter lists subscriptions in one operation:

```sh
$ tblock -Uy
$ tblock --update --with-sync
```
