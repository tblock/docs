> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.0 or later](/2.7.0/filter-lists/repository.html).

# Filter list repository

## Introduction

One of the greatest features of TBlock is [its filter list repository](https://update.tblock.me). It contains over 100+ popular filter lists that are widely used accross the web. It allows users to subscribe to blocklists easily.

## Update the repository

Of course, to have access to the latest filter lists, we need to keep this repository updated. It can be done automatically if you have [automatic updates enabled](../post-installation.md#enable-automatic-updates). Otherwise, you have to update manually.

To update the filter list repository, we have to execute:

```sh
$ sudo tblock -Y
$ sudo tblock --sync
```

With the GUI, the repository can be updated by clicking on the `Manage filter lists` button in the main window, and then clicking on the button labeled `Sync index`.

## Explore the repository

**List all filter lists available in the repository**
```sh
$ tblock -Lw
$ tblock --list --on-repo
```

**Find matches across the filter lists available in the repository**
```sh
$ tblock -Qw "query"
$ tblock --search --on-repo "query"
```

**Getting more information about a specific filter list**
```sh
$ tblock -I some-filter-list
$ tblock --info some-filter-list
```
