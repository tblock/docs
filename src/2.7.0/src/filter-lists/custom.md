> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.3 or later](/2.7.3/filter-lists/custom.html).

# Custom filter lists

Even though there is a large variety of filter lists in the filter list repository, it is possible that we want to subscribe to a filter list that isn't included in it.

## Subscribe to a custom filter list

To subscribe to a custom filter list, we need to assign an ID (or name) to it. To ensure that no filter list has the same ID, it is recommended to use one that begins with `custom-`. 

Here, we'll add a custom filter list from an URL and name it `custom-example`:

```sh
$ tblock -C custom-example https://example.org/list.txt
$ tblock --add-custom custom-example https://example.org/list.txt
```

Here, we'll add a custom filter list from a local file and name it `custom-local`:

```sh
$ tblock -C custom-local /home/user/Downloads/list.txt
$ tblock --add-custom custom-example /home/user/Downloads/list.txt
```

> **&#9888;&#65039; Warning** \
> If you later delete the local file, TBlock will no longer be able to update this filter list. If you move this file to a new location, you'll need to remove the filter list and to add another one with the new access path, since it is currently not possible to change the source of a filter list.

## Specify the filter list format

TBlock supports most filter lists formats. You can list all supported formats with:

```sh
$ tblockc -l
$ tblockc --list-syntax
```

When subscribing to a custom filter list, TBlock will try to detect its format. However, this method has the following disadvantages:

- It slows down the process of subscribing.
- It can incorrectly detect the format, causing all rules to be skipped.

If we know the format of the filter list that we are adding, it is usually better to specify it:

```sh
$ tblock -C custom-example https://example.org/list.txt -x adblockplus
$ tblock --add-custom custom-example https://example.org/list.txt --custom-syntax=adblockplus
```

## Remove a custom filter list

Custom filter lists can be removed in the same way other filter lists are removed. Note that a custom filter list will also have its ID (or name) deleted when it is removed. It means that if we remove `custom-example` and want to subscribe to it again, we'll have to execute again the steps written in the [chapter about subscribing to custom filter lists](#subscribe-to-a-custom-filter-list).

We can list all custom filter lists with this command:

```sh
$ tblock -Lc
$ tblock --list --custom
```

To remove a custom filter list:

```sh
$ tblock -R custom-example
$ tblock --remove custom-example
```

## Rename a custom filter list

Sometimes, we need to rename a custom filter list (for example if it conflicts with a filter list included in the repository).

We can easily to that:

```sh
$ tblock -N custom-example custom-example-new
$ tblock --rename custom-example custom-example-new
```
