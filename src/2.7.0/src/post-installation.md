> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.3 or later](/2.7.3/post-installation.html).

# Post-installation instructions

## Set up the protection

Once TBlock is installed, we need to configure it so that is suits our needs. The first thing to do is to choose the level of protection to use. 

If you use the GUI, a wizard will guide us through the process of setting everything up when launched for the first time. If we want to use the command-line, we have to run:

```sh
$ sudo tblock -1
$ sudo tblock --init
```

Next we should see the following prompt on our screen:

```text
> Welcome to TBlock!


:: Please choose the level that suits your needs the best:

[0] None: lets you configure everything yourself
[1] Light: light protection, some ads and trackers won't be blocked
[2] Balanced: perfect solution for regular users
[3] Aggressive: powerful, yet it may break some web pages

Your choice [default=2]: 
```

Once our choice is made, we just have to type the number corresponding to the profile we want to use. 
Then we need to select which additional components to block. Once this is done, the program will set everything up for you.

## Enable automatic updates

One of TBlock's great features is its ability to automatically update blocklists. This can be achieved by using the built-in daemon, `tblockd`. To enable automatic updates, we need to enable and activate the system service called `tblockd`.

> **&#8505;&#65039; Info** \
> This feature is still work-in-progress for Windows and macOS. Manual updates are required for both of these platforms. [More information is available here](https://codeberg.org/tblock/tblock/issues/51).

> **&#10067; How to tell if I'm using systemd?** \
> If you have no idea what systemd is, you're probably using it.

**With systemd**

```sh
$ sudo systemctl enable tblockd --now
```

**With OpenRC**

```sh
$ sudo rc-update add tblockd
$ sudo rc-service tblockd start
```

**Manually**

```sh
$ sudo tblockd --daemon
```
