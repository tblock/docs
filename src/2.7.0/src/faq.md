> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.3 or later](/2.7.3/faq.html).

# FAQ

## Why do I still see ads after installing and setting up TBlock?

Since TBlock uses the hosts file, it gives you a systemwide protection. However, it can't block specific scripts like browser addons do. It means that you may still see some sneaky ads (for example, TBlock is not able to block YouTube ads). 

That's why we recommend to install [uBlock Origin](https://github.com/gorhill/uBlock) in addition to TBlock.

## Is there a graphical interface for TBlock?

YES! Unfortunately, it's only available on Linux for now, but it is under development for other platforms. You can learn how to install it in the [installation guide](./installation/index.md).

## I think I found an issue. What should I do?

Tell us! &#128513; You may want to have a look at [this document](https://codeberg.org/tblock/tblock/src/branch/main/CONTRIBUTING.md) that contains useful information for reporting issues.
