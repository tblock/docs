> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.3 or later](/2.7.3/installation/debian.html).

# Install on Debian

> **&#128221; Note** \
> Since version 2.7.0, the package `tblock` contains system services for the following init systems: `systemd`, `openrc`, `runit` and `dinit`.

Debian packages for both `tblock` and `tblock-gui` are provided on the releases pages of the repositories.

- [Click here](https://codeberg.org/tblock/tblock/releases) to view releases of `tblock`
- [Click here](https://codeberg.org/tblock/tblock-gui/releases) to view releases of `tblock-gui`

After downloading the package(s), we need to install them:

```sh
$ sudo apt install ./tblock_*.deb
$ sudo apt install ./tblock-gui_*.deb
```

> **&#128994; Tip** \
> The package `tblock-gui` depends on `tblock`. You should install `tblock` before `tblock-gui`.
