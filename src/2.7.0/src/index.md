> **&#9888;&#65039; Warning** \
> This documentation is outdated. Please visit [the documentation for TBlock 2.7.3 or later](/2.7.3/)

![TBlock banner](./banner.png)

# Welcome to TBlock documentation!

This is the place where you can learn how to use TBlock and how it works. If this is your first time using TBlock, we recommend you to read this documentation from the beginning, but you can also select the chapter you are interested into.

While this documentation focuses more on the command-line tool `tblock`, it also contains installation instructions and some tips for the GUI.

> **&#8505;&#65039; Info** \
> This documentation is available for TBlock 2.7.0 or later. Information for older versions may be inaccurate.
