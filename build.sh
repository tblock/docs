if [ -d output ]
then 
    rm -rf output/*.* output/*/
fi
mkdir -p output
for x in src/*/
do 
    cd "$x"
    mdbook build
    cp -rv book ../../output/$(basename $x)
    cd ../..
done
for x in static/*.*
do
    cp -v "$x" output
done
cp -v LICENSE output/LICENSE.txt
cp -v .domains output/.domains
